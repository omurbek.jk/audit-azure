coreo_azure_rule "azure-virtual-extension-linuxasm-enabled" do
  action :define
  service :compute
  description "Check virtual machine linuxasm extension"
  category "Security"
  suggested_action "Ensure that VM's have linuxasm extension configured."
  level "Warning"
  objectives [ "virtual_machines.list_all", "virtual_machine_extensions.get" ]
  call_modifiers [ {},
    { :arg0 => "resources.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "resources.id.%r{/virtualMachines/([^/]+)/}",
      :arg2 => "resources.id.%r{/extensions/([^/]+)/?}"
    }
  ]
  audit_objects [ "", "virtual_machine_extension_type" ]
  operators ["", "!="]
  raise_when ["", "LinuxAsm"]
  id_map ["static.VirtualMachine", "object.id"]
end

coreo_azure_rule "azure-virtual-dynamic-private-ip" do
  action :define
  service :compute
  description "Check virtual machine network profile"
  category "Security"
  suggested_action "Ensure that VM's have dynamic private IP assignment."
  level "Warning"
  objectives [ "virtual_machines.list_all", ":network.network_interfaces.get" ]
  call_modifiers [ {},
    { :arg0 => "network_profile.network_interfaces.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "network_profile.network_interfaces.id.%r{/networkInterfaces/([^/]+)/?}" }
  ]
  audit_objects [ "", "ip_configurations.private_ipallocation_method" ]
  operators ["", "!~"]
  raise_when ["", /Dynamic/i]
  id_map ["static.VirtualMachine", "object.id"]
end

coreo_azure_rule "azure-virtual-no-ip-forwarding" do
  action :define
  service :compute
  description "Check virtual machine network profile"
  category "Security"
  suggested_action "Ensure that VM's do not allow IP forwarding."
  level "Warning"
  objectives [ "virtual_machines.list_all", ":network.network_interfaces.get" ]
  call_modifiers [ {},
    { :arg0 => "network_profile.network_interfaces.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "network_profile.network_interfaces.id.%r{/networkInterfaces/([^/]+)/?}" }
  ]
  audit_objects [ "", "enable_ipforwarding" ]
  operators ["", "!="]
  raise_when ["", false]
  id_map ["static.VirtualMachine", "object.id"]
end

coreo_azure_rule "azure-virtual-network-no-privileged-ports" do
  action :define
  service :compute
  description "Check virtual machine network profile"
  category "Security"
  suggested_action "Ensure that VM's do not allow IP forwarding."
  level "Warning"
  objectives ["virtual_machines.list_all",
              ":network.network_interfaces.get",
              ":network.network_security_groups.get",
              "",
              "",
              ":network.security_rules.get"]
  call_modifiers [ {},
    { :arg0 => "network_profile.network_interfaces.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "network_profile.network_interfaces.id.%r{/networkInterfaces/([^/]+)/?}" },
    { :arg0 => "network_security_group.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "network_security_group.id.%r{/networkSecurityGroups/([^/]+)/?}" },
    { :arg0 => "security_rules.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "security_rules.id.%r{/networkSecurityGroups/([^/]+)/}",
      :arg2 => "security_rules.id.%r{/securityRules/([^/]+)/?}" },
    {},
    {}
  ]
  audit_objects ["", "", "", "access", "direction", "destination_port_range"]
  operators ["", "", "", "==", "==", "in"]
  raise_when ["", "", "", "Allow", "Inbound", "0..1024"]
  id_map ["static.VirtualMachine", "object.id"]
end

coreo_azure_rule "azure-virtual-network-no-wildcard-ports" do
  action :define
  service :compute
  description "Check virtual machine network profile"
  category "Security"
  suggested_action "Ensure that VM's do not allow IP forwarding."
  level "Warning"
  objectives ["virtual_machines.list_all",
              ":network.network_interfaces.get",
              ":network.network_security_groups.get",
              "",
              "",
              ":network.security_rules.get"]
  call_modifiers [ {},
    { :arg0 => "network_profile.network_interfaces.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "network_profile.network_interfaces.id.%r{/networkInterfaces/([^/]+)/?}" },
    { :arg0 => "network_security_group.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "network_security_group.id.%r{/networkSecurityGroups/([^/]+)/?}" },
    { :arg0 => "security_rules.id.%r{/resourceGroups/([^/]+)/}",
      :arg1 => "security_rules.id.%r{/networkSecurityGroups/([^/]+)/}",
      :arg2 => "security_rules.id.%r{/securityRules/([^/]+)/?}" },
    {},
    {}
  ]
  audit_objects ["", "", "", "access", "direction", "destination_port_range"]
  operators ["", "", "", "==", "==", "=="]
  raise_when ["", "", "", "Allow", "Inbound", "*"]
  id_map ["static.VirtualMachine", "object.id"]
end

coreo_azure_rule "azure-providers-check" do
  action :define
  service :resources
  description "Check Resources"
  category "Security"
  suggested_action "Review Resource Providers to ensure something."
  level "Warning"
  objectives ["providers.list"]
  audit_objects [""]
  formulas  ["count"]
  operators ["=="]
  raise_when [0]
  id_map "static.Providers"
end

coreo_azure_rule "azure-security-provider-registered" do
  action :define
  service :resources
  description "Check Microsoft.Security Provider"
  category "Security"
  suggested_action "Ensure Microsoft.Security Provider is registered."
  level "Warning"
  objectives ["", "providers.list"]
  audit_objects ["namespace", "registration_state"]
  operators ["==", "!="]
  raise_when ["Microsoft.Security", "Registered"]
  id_map ["static.Provider", "object.id"]
end

coreo_azure_rule "azure-microsoft-only-providers" do
  action :define
  service :resources
  description "Check for Microsoft. only Providers"
  category "Security"
  suggested_action "Ensure that only Microsoft.* Providers are registered."
  level "Warning"
  objectives ["", "providers.list"]
  audit_objects ["namespace", "registration_state"]
  operators ["!~", "=="]
  raise_when [/\aMicrosoft\./, "Registered"]
  id_map ["static.Provider", "object.id"]
end

coreo_azure_rule_runner "azure-compute-best-practices" do
  action :run
  service :compute
  rules ${AUDIT_AZURE_COMPUTE_ALERT_LIST}
end

coreo_azure_rule_runner "azure-resources-best-practices" do
  action :run
  service :resources
  rules ${AUDIT_AZURE_RESOURCES_ALERT_LIST}
end
