audit Azure
============================
This stack will monitor Azure and alert on things CloudCoreo developers think are violations of best practices


## Description
This repo is designed to work with CloudCoreo. It will monitor Azure against best practices for you and send a report to the email address designated by the config.yaml AUDIT_AZURE_ALERT_RECIPIENT value


## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/audit-azure/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

**None**


## Required variables with default

### `AUDIT_AZURE_ALLOW_EMPTY`:
  * description: Would you like to receive empty reports? Options - true / false. Default is false.
  * default: false

### `AUDIT_AZURE_SEND_ON`:
  * description: Send reports always or only when there is a change? Options - always / change. Default is change.
  * default: change


## Optional variables with default

### `AUDIT_AZURE_COMPUTE_ALERT_LIST`:
  * description: Which rules would you like to run? Possible values are azure-virtual-extension-linuxasm-enabled,azure-virtual-dynamic-private-ip,azure-virtual-no-ip-forwarding,azure-virtual-network-no-privileged-ports,azure-virtual-network-no-wildcard-ports
  * default: azure-virtual-extension-linuxasm-enabled, azure-virtual-dynamic-private-ip, azure-virtual-no-ip-forwarding, azure-virtual-network-no-privileged-ports, azure-virtual-network-no-wildcard-ports

### `AUDIT_AZURE_RESOURCES_ALERT_LIST`:
  * description: Which rules would you like to run? Possible values are azure-providers-check,azure-security-provider-registered,azure-microsoft-only-providers
  * default: azure-providers-check, azure-security-provider-registered, azure-microsoft-only-providers

### `AUDIT_AZURE_OWNER_TAG`:
  * description: Enter an Azure tag whose value is an email address of owner of the object. (Optional)
  * default: NOT_A_TAG


## Optional variables with no default

### `AUDIT_AZURE_ALERT_RECIPIENT`:
  * description: Enter the email address(es) that will receive notifiers. If more than one, separate each with a comma.

### `AUDIT_AZURE_REGIONS`:
  * description: List of Azure regions to check. Default is all regions. Choices are test-1 test-2

## Tags
1. Audit
1. Best Practices
1. Alert
1. Azure


## Categories
1. Azure Audit Policies



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/audit-azure/master/images/diagram.png "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/audit-azure/master/images/icon.png "icon")


